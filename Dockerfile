FROM ubuntu:xenial
MAINTAINER https://gitlab.com/alelec/docker_python_manydarling

RUN apt -qq update && \
    apt-get -qq install -y build-essential git-core cmake clang bison flex xz-utils \
                       libfuse-dev libudev-dev pkg-config libc6-dev-i386 \
                       linux-headers-generic

RUN git clone --quiet --recursive https://github.com/darlinghq/darling.git >/dev/null 2>&1
RUN cd darling && mkdir build && cd build && \
    cmake .. -DCMAKE_TOOLCHAIN_FILE=../Toolchain.cmake && \
    make
    
RUN cd /darling/build && make install >/dev/null 2>&1

RUN cd /darling/src/lkm && \
    make && make install

ENTRYPOINT darling shell
